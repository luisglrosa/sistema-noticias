@extends('layouts.app')

@section('titulo','Home')

@section('conteudo')

<div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Noticias</h2>
                <a href="#" class="btn btm-danger float-right"><i class="fas fa-plus"></i>Adicionar</a>
                
            </div>
        </div>
        <div class="row mt-3">
                <div class="col-12">
                  <table class="table table-bordered table-striped">
                      <thead>
                          <tr>
                              <th>3</th>
                              <th>Titulo</th>
                              <th>Subtitulo</th>
                              <th>Data Publicação</th>
                              <th>Ação</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td scope="row">1</td>
                              <td>Palmeira Campeão Mundial de 1951</td>
                              <td>Palmeira Campeão Mundial de 1951</td>
                              <td>14/05/2019 22:19</td>
                              <td>
                                  <a href="#" class="btn btn-secundary"> <i class="fas fa-edit"></i></a>
                                  <a href="#" class="btn btn-sm btn-warning"> <i class="fas fa-edit"></i></a>
                                  <a href="#" class="btn btn-sm btn-danger"> <i class="fas fa-trash"></i></a>
                            
                            
                            </td>
                              
                          </tr>
                         
                      </tbody>
                  </table>
                </div>
        </div>
        
        <div class="row">
            <div class="col-12">
                <nav aria-label="Page navigation">
                  <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                      </a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                      </a>
                    </li>
                  </ul>
                </nav>
            </div>
        </div>

    </div>


@endsection