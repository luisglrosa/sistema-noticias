<?php

//Seção Home

Route::get('/', function () {
    return view('home.index');
});

Route::get('/contato', function () {
    return view('home.contato');
});


//Seção Noticias
Route::get('/tecnologia', function () {
    return view('noticias.index');
});

Route::get('/tecnologia/titulo-noticia', function () {
    return view('noticias.visualizar');
});


//Seção admin

Route::get('/admin/home', function () {
    return view('admin.home.index');

});

//Seção admin/noticias

Route::get('/admin/noticias','Admin\NoticiasController@index');


Route::get('/admin/noticias/cadastrar','Admin\NoticiasController@cadastrar');

Route::get('/admin/noticias/editar','Admin\NoticiasController@editar');

Route::get('/admin/noticias/visualizar','Admin\NoticiasController@visualizar');

route::get('/admin/noticias/deletar','Admin\NoticiasController@deletar');

Route::get('/admin/usuarios/index', function () {
    return view('admin.usuarios.index');
});



Route::get('/admin/usuarios/editar','Admin\UsuariosController@editar');

Route::get('/admin/categorias/index','Admin\CategoriasController@index');

Route::get('/admin/usuarios/visualizar','Admin\UsuariosController@visualizar');

Route::get('/admin/usuarios/cadastrar','Admin\UsuariosController@cadastrar');

Route::get('/admin/usuarios/cadastrar','Admin\UsuariosController@index');


//Seção admin/categoria






//Seção admin/usuario